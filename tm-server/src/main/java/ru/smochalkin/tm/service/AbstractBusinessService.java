package ru.smochalkin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.api.IBusinessService;
import ru.smochalkin.tm.api.service.IConnectionService;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.model.AbstractBusinessEntity;

import java.sql.Connection;
import java.util.List;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    public AbstractBusinessService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    public abstract IBusinessRepository<E> getRepository(@NotNull Connection connection);

    @Override
    @SneakyThrows
    public void clear(final String userId) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            try {
                repository.clear(userId);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            return repository.findAllByUserId(userId);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<E> findAll(@NotNull final String userId, @NotNull final String sort) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            return repository.findAll(userId, sort);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public E findById(@NotNull final String userId, @Nullable final String id) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            return repository.findById(userId, id);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public E findByName(@NotNull final String userId, final @NotNull String name) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            return repository.findByName(userId, name);
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public E findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            return repository.findByIndex(userId, index);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @NotNull String id) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            try {
                repository.removeById(userId, id);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, final @NotNull String name) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            try {
                repository.removeByName(userId, name);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            try {
                repository.removeByIndex(userId, index);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(@NotNull final String id, @NotNull final String name, @Nullable final String desc) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            try {
                repository.updateById(id, name, desc);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @Nullable final String desc
    ) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            try {
                repository.updateById(userId, id, name, desc);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @Nullable final String desc
    ) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            try {
                repository.updateByIndex(userId, index, name, desc);
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusById(
            @NotNull String userId,
            @NotNull final String id,
            @NotNull final String strStatus
    ) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            try {
                repository.updateStatusById(userId, id, Status.getStatus(strStatus));
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String strStatus
    ) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            try {
                repository.updateStatusByName(userId, name, Status.getStatus(strStatus));
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public void updateStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String strStatus
    ) {
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            try {
                repository.updateStatusByIndex(userId, index, Status.getStatus(strStatus));
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            connection.commit();
        }
    }

    @Override
    @SneakyThrows
    public boolean isNotIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) return true;
        try (@NotNull final Connection connection = connectionService.getConnection()) {
            @NotNull final IBusinessRepository<E> repository = getRepository(connection);
            return index >= repository.getCountByUser(userId);
        }
    }

}
