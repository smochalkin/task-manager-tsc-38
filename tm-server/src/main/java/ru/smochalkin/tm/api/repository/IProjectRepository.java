package ru.smochalkin.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.smochalkin.tm.api.IBusinessRepository;
import ru.smochalkin.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {
}
