package ru.smochalkin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.repository.IProjectRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.Project;
import ru.smochalkin.tm.service.ConnectionService;
import ru.smochalkin.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    private static final int ENTRY_COUNT = 5;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private Connection connection;

    @NotNull
    private IProjectRepository projectRepository;

    private int repositorySize;

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        connection = connectionService.getConnection();
        projectRepository = new ProjectRepository(connection);
        for (int i = 1; i <= ENTRY_COUNT; i++) {
            @NotNull final String userId;
            if ((i < ENTRY_COUNT / 2))
                userId = USER_ID_1;
            else
                userId = USER_ID_2;
            try {
                projectRepository.add(userId, "test" + i, "test" + i);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        }
        repositorySize = projectRepository.getCount();
    }

    @After
    @SneakyThrows
    public void end() {
        for (int i = 1; i <= ENTRY_COUNT; i++) {
            try {
                projectRepository.removeByName(USER_ID_1, "test" + i);
                projectRepository.removeByName(USER_ID_2, "test" + i);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        }
        projectRepository.removeByName(USER_ID_1, "testAdd");
        connection.commit();
    }

    @Test
    @SneakyThrows
    public void addTest() {
        projectRepository.add(USER_ID_1, "testAdd", "testAdd");
        connection.commit();
        @NotNull final Project project = projectRepository.findByName(USER_ID_1, "testAdd");
        Assert.assertEquals("testAdd", project.getName());
    }

    @Test
    public void clearByUserTest() {
        projectRepository.clear(USER_ID_1);
        Assert.assertEquals(0, projectRepository.findAllByUserId(USER_ID_1).size());
    }

    @Test
    public void findAllTest() {
        @NotNull final List<Project> actualProjectList = projectRepository.findAll();
        Assert.assertEquals(repositorySize, actualProjectList.size());
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final List<Project> projects = projectRepository.findAllByUserId(USER_ID_1);
        Assert.assertEquals(1, projects.size());
        Assert.assertEquals(USER_ID_1, projects.get(0).getUserId());
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> projectRepository.findById(UUID.randomUUID().toString()));
        @NotNull final Project projectByName = projectRepository.findByName(USER_ID_1, "test1");
        @NotNull final Project projectById = projectRepository.findById(USER_ID_1, projectByName.getId());
        Assert.assertEquals("test1", projectById.getName());
    }

    @Test
    public void findByNameTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> projectRepository.findByName(USER_ID_1, "not found"));
        @NotNull final Project project = projectRepository.findByName(USER_ID_1, "test1");
        Assert.assertEquals("test1", project.getName());
    }

    @Test
    public void findByIndexTest() {
        @NotNull final Project project = projectRepository.findByIndex(USER_ID_1, 0);
        Assert.assertEquals(USER_ID_1, project.getUserId());
    }

    @Test
    public void existsByIdTest() {
        @NotNull final Project projectByName = projectRepository.findByName(USER_ID_1, "test1");
        @NotNull final Project projectById = projectRepository.findById(USER_ID_1, projectByName.getId());
        Assert.assertTrue(projectRepository.existsById(projectById.getId()));
        Assert.assertFalse(projectRepository.existsById("notId"));
    }

    @Test
    public void getCountTest() {
        Assert.assertEquals(repositorySize, projectRepository.getCount());
    }

    @Test
    @SneakyThrows
    public void removeByIdTest() {
        @NotNull final Project project = projectRepository.findByName(USER_ID_1, "test1");
        projectRepository.removeById(project.getId());
        connection.commit();
        Assert.assertThrows(EntityNotFoundException.class,
                () -> projectRepository.findById(project.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByIdAndByUserIdTest() {
        @NotNull final Project project = projectRepository.findByName(USER_ID_1, "test1");
        projectRepository.removeById(project.getId());
        connection.commit();
        Assert.assertThrows(EntityNotFoundException.class,
                () -> projectRepository.findById(USER_ID_1, project.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByNameTest() {
        projectRepository.removeByName(USER_ID_1, "test1");
        connection.commit();
        Assert.assertThrows(EntityNotFoundException.class,
                () -> projectRepository.findByName(USER_ID_1, "test1"));
    }

    @Test
    @SneakyThrows
    public void removeByIndexTest() {
        @NotNull final Project project1 = projectRepository.findByIndex(USER_ID_2, 0);
        projectRepository.removeByIndex(USER_ID_2, 0);
        connection.commit();
        @NotNull final Project project2 = projectRepository.findByIndex(USER_ID_2, 0);
        Assert.assertNotEquals(project1.getId(), project2.getId());
    }

    @Test
    @SneakyThrows
    public void updateByIdTest() {
        @NotNull final Project project1 = projectRepository.findByName(USER_ID_1, "test1");
        @NotNull final String oldName = project1.getName();
        @Nullable final String oldDesc = project1.getDescription();
        projectRepository.updateById(project1.getId(), oldName, oldDesc);
        connection.commit();
        @NotNull final Project project2 = projectRepository.findById(USER_ID_1, project1.getId());
        Assert.assertEquals(project1.getName(), project2.getName());
        Assert.assertEquals(project2.getDescription(), project2.getDescription());
    }

    @Test
    @SneakyThrows
    public void updateByIdAndByUserIdTest() {
        @NotNull final Project project1 = projectRepository.findByName(USER_ID_1, "test1");
        @NotNull final String oldName = project1.getName();
        @Nullable final String oldDesc = project1.getDescription();
        projectRepository.updateById(USER_ID_1, project1.getId(), oldName, oldDesc);
        connection.commit();
        @NotNull final Project project2 = projectRepository.findById(USER_ID_1, project1.getId());
        Assert.assertEquals(project1.getName(), project2.getName());
        Assert.assertEquals(project2.getDescription(), project2.getDescription());
    }

    @Test
    @SneakyThrows
    public void updateByIndexTest() {
        @NotNull final Project project1 = projectRepository.findByIndex(USER_ID_1, 0);
        @NotNull final String oldName = project1.getName();
        @Nullable final String oldDesc = project1.getDescription();
        projectRepository.updateByIndex(USER_ID_1, 0, oldName, oldDesc);
        connection.commit();
        @NotNull final Project project2 = projectRepository.findByIndex(USER_ID_1, 0);
        Assert.assertEquals(project1.getName(), project2.getName());
        Assert.assertEquals(project2.getDescription(), project2.getDescription());
    }

    @Test
    public void updateStatusByIdTest() {
        @NotNull final Project project1 = projectRepository.findByName(USER_ID_1, "test1");
        @Nullable final Date startDate = project1.getStartDate();
        projectRepository.updateStatusById(USER_ID_1, project1.getId(), Status.IN_PROGRESS);
        @NotNull final Project project2 = projectRepository.findById(project1.getId());
        Assert.assertNotEquals(Status.IN_PROGRESS, project1.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project2.getStatus());
        Assert.assertNotEquals(startDate, project2.getStartDate());
    }

    @Test
    public void updateStatusByNameTest() {
        @NotNull final Project project1 = projectRepository.findByName(USER_ID_1, "test1");
        @Nullable final Date startDate = project1.getStartDate();
        projectRepository.updateStatusByName(USER_ID_1, project1.getName(), Status.IN_PROGRESS);
        @NotNull final Project project2 = projectRepository.findById(project1.getId());
        Assert.assertNotEquals(Status.IN_PROGRESS, project1.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project2.getStatus());
        Assert.assertNotEquals(startDate, project2.getStartDate());
    }

    @Test
    public void updateStatusByIndexTest() {
        @NotNull final Project project1 = projectRepository.findByIndex(USER_ID_1, 0);
        @Nullable final Date startDate = project1.getStartDate();
        projectRepository.updateStatusByIndex(USER_ID_1, 0, Status.IN_PROGRESS);
        @NotNull final Project project2 = projectRepository.findById(project1.getId());
        Assert.assertNotEquals(Status.IN_PROGRESS, project1.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, project2.getStatus());
        Assert.assertNotEquals(startDate, project2.getStartDate());
    }

    @Test
    public void getCountByUserTest() {
        Assert.assertEquals(1, projectRepository.getCountByUser(USER_ID_1));
    }

}
