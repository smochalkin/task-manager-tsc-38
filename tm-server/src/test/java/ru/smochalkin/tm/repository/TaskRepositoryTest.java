package ru.smochalkin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.smochalkin.tm.api.repository.ITaskRepository;
import ru.smochalkin.tm.enumerated.Status;
import ru.smochalkin.tm.exception.entity.EntityNotFoundException;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.service.ConnectionService;
import ru.smochalkin.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class TaskRepositoryTest {

    private static final int ENTRY_COUNT = 5;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String PROJECT_ID_1 = UUID.randomUUID().toString();

    private static final String PROJECT_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private Connection connection;

    @NotNull
    private ITaskRepository taskRepository;

    private int repositorySize;

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        connection = connectionService.getConnection();
        taskRepository = new TaskRepository(connection);
        for (int i = 1; i <= ENTRY_COUNT; i++) {
            @NotNull final String userId;
            if ((i < ENTRY_COUNT / 2))
                userId = USER_ID_1;
            else
                userId = USER_ID_2;
            try {
                taskRepository.add(userId, "test" + i, "test" + i);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        }
        repositorySize = taskRepository.getCount();
    }

    @After
    @SneakyThrows
    public void end() {
        for (int i = 1; i <= ENTRY_COUNT; i++) {
            try {
                taskRepository.removeByName(USER_ID_1, "test" + i);
                taskRepository.removeByName(USER_ID_2, "test" + i);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
        }
        taskRepository.removeByName(USER_ID_1, "testAdd");
        connection.commit();
    }

    @Test
    @SneakyThrows
    public void addTest() {
        taskRepository.add(USER_ID_1, "testAdd", "testAdd");
        connection.commit();
        @NotNull final Task task = taskRepository.findByName(USER_ID_1, "testAdd");
        Assert.assertEquals("testAdd", task.getName());
    }

    @Test
    public void clearByUserTest() {
        taskRepository.clear(USER_ID_1);
        Assert.assertEquals(0, taskRepository.findAllByUserId(USER_ID_1).size());
    }

    @Test
    public void findAllTest() {
        @NotNull final List<Task> actualTaskList = taskRepository.findAll();
        Assert.assertEquals(repositorySize, actualTaskList.size());
    }

    @Test
    public void findAllByUserIdTest() {
        @NotNull final List<Task> tasks = taskRepository.findAllByUserId(USER_ID_1);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(USER_ID_1, tasks.get(0).getUserId());
    }

    @Test
    public void findByIdTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> taskRepository.findById(UUID.randomUUID().toString()));
        @NotNull final Task taskByName = taskRepository.findByName(USER_ID_1, "test1");
        @NotNull final Task taskById = taskRepository.findById(USER_ID_1, taskByName.getId());
        Assert.assertEquals("test1", taskById.getName());
    }

    @Test
    public void findByNameTest() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> taskRepository.findByName(USER_ID_1, "not found"));
        @NotNull final Task task = taskRepository.findByName(USER_ID_1, "test1");
        Assert.assertEquals("test1", task.getName());
    }

    @Test
    public void findByIndexTest() {
        @NotNull final Task task = taskRepository.findByIndex(USER_ID_1, 0);
        Assert.assertEquals(USER_ID_1, task.getUserId());
    }

    @Test
    public void existsByIdTest() {
        @NotNull final Task taskByName = taskRepository.findByName(USER_ID_1, "test1");
        @NotNull final Task taskById = taskRepository.findById(USER_ID_1, taskByName.getId());
        Assert.assertTrue(taskRepository.existsById(taskById.getId()));
        Assert.assertFalse(taskRepository.existsById("notId"));
    }

    @Test
    public void getCountTest() {
        Assert.assertEquals(repositorySize, taskRepository.getCount());
    }

    @Test
    @SneakyThrows
    public void removeByIdTest() {
        @NotNull final Task task = taskRepository.findByName(USER_ID_1, "test1");
        taskRepository.removeById(task.getId());
        connection.commit();
        Assert.assertThrows(EntityNotFoundException.class,
                () -> taskRepository.findById(task.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByIdAndByUserIdTest() {
        @NotNull final Task task = taskRepository.findByName(USER_ID_1, "test1");
        taskRepository.removeById(task.getId());
        connection.commit();
        Assert.assertThrows(EntityNotFoundException.class,
                () -> taskRepository.findById(USER_ID_1, task.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByNameTest() {
        taskRepository.removeByName(USER_ID_1, "test1");
        connection.commit();
        Assert.assertThrows(EntityNotFoundException.class,
                () -> taskRepository.findByName(USER_ID_1, "test1"));
    }

    @Test
    @SneakyThrows
    public void removeByIndexTest() {
        @NotNull final Task task1 = taskRepository.findByIndex(USER_ID_2, 0);
        taskRepository.removeByIndex(USER_ID_2, 0);
        connection.commit();
        @NotNull final Task task2 = taskRepository.findByIndex(USER_ID_2, 0);
        Assert.assertNotEquals(task1.getId(), task2.getId());
    }

    @Test
    @SneakyThrows
    public void updateByIdTest() {
        @NotNull final Task task1 = taskRepository.findByName(USER_ID_1, "test1");
        @NotNull final String oldName = task1.getName();
        @Nullable final String oldDesc = task1.getDescription();
        taskRepository.updateById(task1.getId(), oldName, oldDesc);
        connection.commit();
        @NotNull final Task task2 = taskRepository.findById(USER_ID_1, task1.getId());
        Assert.assertEquals(task1.getName(), task2.getName());
        Assert.assertEquals(task2.getDescription(), task2.getDescription());
    }

    @Test
    @SneakyThrows
    public void updateByIdAndByUserIdTest() {
        @NotNull final Task task1 = taskRepository.findByName(USER_ID_1, "test1");
        @NotNull final String oldName = task1.getName();
        @Nullable final String oldDesc = task1.getDescription();
        taskRepository.updateById(USER_ID_1, task1.getId(), oldName, oldDesc);
        connection.commit();
        @NotNull final Task task2 = taskRepository.findById(USER_ID_1, task1.getId());
        Assert.assertEquals(task1.getName(), task2.getName());
        Assert.assertEquals(task2.getDescription(), task2.getDescription());
    }

    @Test
    @SneakyThrows
    public void updateByIndexTest() {
        @NotNull final Task task1 = taskRepository.findByIndex(USER_ID_1, 0);
        @NotNull final String oldName = task1.getName();
        @Nullable final String oldDesc = task1.getDescription();
        taskRepository.updateByIndex(USER_ID_1, 0, oldName, oldDesc);
        connection.commit();
        @NotNull final Task task2 = taskRepository.findByIndex(USER_ID_1, 0);
        Assert.assertEquals(task1.getName(), task2.getName());
        Assert.assertEquals(task2.getDescription(), task2.getDescription());
    }

    @Test
    public void updateStatusByIdTest() {
        @NotNull final Task task1 = taskRepository.findByName(USER_ID_1, "test1");
        @Nullable final Date startDate = task1.getStartDate();
        taskRepository.updateStatusById(USER_ID_1, task1.getId(), Status.IN_PROGRESS);
        @NotNull final Task task2 = taskRepository.findById(task1.getId());
        Assert.assertNotEquals(Status.IN_PROGRESS, task1.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task2.getStatus());
        Assert.assertNotEquals(startDate, task2.getStartDate());
    }

    @Test
    public void updateStatusByNameTest() {
        @NotNull final Task task1 = taskRepository.findByName(USER_ID_1, "test1");
        @Nullable final Date startDate = task1.getStartDate();
        taskRepository.updateStatusByName(USER_ID_1, task1.getName(), Status.IN_PROGRESS);
        @NotNull final Task task2 = taskRepository.findById(task1.getId());
        Assert.assertNotEquals(Status.IN_PROGRESS, task1.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task2.getStatus());
        Assert.assertNotEquals(startDate, task2.getStartDate());
    }

    @Test
    public void updateStatusByIndexTest() {
        @NotNull final Task task1 = taskRepository.findByIndex(USER_ID_1, 0);
        @Nullable final Date startDate = task1.getStartDate();
        taskRepository.updateStatusByIndex(USER_ID_1, 0, Status.IN_PROGRESS);
        @NotNull final Task task2 = taskRepository.findById(task1.getId());
        Assert.assertNotEquals(Status.IN_PROGRESS, task1.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task2.getStatus());
        Assert.assertNotEquals(startDate, task2.getStartDate());
    }

    @Test
    public void getCountByUserTest() {
        Assert.assertEquals(1, taskRepository.getCountByUser(USER_ID_1));
    }

    @Test
    @SneakyThrows
    public void bindTaskByIdTest() {
        @NotNull final Task task1 = taskRepository.findByName(USER_ID_1, "test1");
        Assert.assertNotEquals(PROJECT_ID_1, task1.getProjectId());
        @NotNull final String taskId = task1.getId();
        taskRepository.bindTaskById(PROJECT_ID_1, taskId);
        connection.commit();
        @NotNull final Task task2 = taskRepository.findByName(USER_ID_1, "test1");
        Assert.assertEquals(PROJECT_ID_1, task2.getProjectId());
    }

    @Test
    @SneakyThrows
    public void bindTaskByIdAndByUserIdTest() {
        @NotNull final Task task1 = taskRepository.findByName(USER_ID_1, "test1");
        Assert.assertNotEquals(PROJECT_ID_1, task1.getProjectId());
        @NotNull final String taskId = task1.getId();
        taskRepository.bindTaskById(USER_ID_1, PROJECT_ID_1, taskId);
        connection.commit();
        @NotNull final Task task2 = taskRepository.findByName(USER_ID_1, "test1");
        Assert.assertEquals(PROJECT_ID_1, task2.getProjectId());
    }

    @Test
    @SneakyThrows
    public void unbindTaskByIdTest() {
        @NotNull final Task task1 = taskRepository.findByName(USER_ID_1, "test1");
        taskRepository.bindTaskById(PROJECT_ID_1, task1.getId());
        taskRepository.unbindTaskById(PROJECT_ID_1, task1.getId());
        connection.commit();
        @NotNull final Task task2 = taskRepository.findByName(USER_ID_1, "test1");
        Assert.assertNull(task2.getProjectId());
    }

    @Test
    @SneakyThrows
    public void unbindTaskByIdAndByUserIdTest() {
        @NotNull final Task task1 = taskRepository.findByName(USER_ID_1, "test1");
        taskRepository.bindTaskById(USER_ID_1, PROJECT_ID_1, task1.getId());
        taskRepository.unbindTaskById(USER_ID_1, PROJECT_ID_1, task1.getId());
        connection.commit();
        @NotNull final Task task2 = taskRepository.findByName(USER_ID_1, "test1");
        Assert.assertNull(task2.getProjectId());
    }

    @Test
    public void findTasksByProjectIdTest() {
        @NotNull final Task task1 = taskRepository.findByName(USER_ID_1, "test1");
        taskRepository.bindTaskById(USER_ID_1, PROJECT_ID_1, task1.getId());
        Assert.assertEquals(1, taskRepository.findTasksByProjectId(PROJECT_ID_1).size());
    }

    @Test
    public void findTasksByProjectIdUserIdTest() {
        @NotNull final Task task1 = taskRepository.findByName(USER_ID_1, "test1");
        taskRepository.bindTaskById(USER_ID_1, PROJECT_ID_1, task1.getId());
        Assert.assertEquals(1, taskRepository.findTasksByProjectId(USER_ID_1, PROJECT_ID_1).size());
    }

    @Test
    public void removeTasksByProjectIdTest() {
        @NotNull final Task task1 = taskRepository.findByName(USER_ID_1, "test1");
        taskRepository.bindTaskById(USER_ID_1, PROJECT_ID_1, task1.getId());
        Assert.assertEquals(1, taskRepository.findTasksByProjectId(USER_ID_1, PROJECT_ID_1).size());
        taskRepository.removeTasksByProjectId(PROJECT_ID_1);
        Assert.assertEquals(0, taskRepository.findTasksByProjectId(PROJECT_ID_1).size());
    }

}
